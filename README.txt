This module is designed to make it easy to create a page title pane and set
a <title> tag for your panels page(s).

Installation
1. Unpack the module in your Drupal sites/all/modules directory.
2. Enable the module in Admin menu > Site building > Modules (admin/modules).

The module is relatively easy to use, here are detailed instructions.

Usage
1. In your variant set the title type on the content tab to "no title".
2. In a region click the gear and select
   Add content > Page Title > panels Page Title.
3. For the setting you most likely will leave "override title" and "Title tag"
   alone.
4. Enter a css id if you want, the css class defaults to "title", feel free to
   change it.
5. Enter you page title, this will also set your <title> tag using the drupal
   default of page_title | site_name.
   You can use substitutions from the substitutions dropdown, if you want to use
   tokens add a token context to your panel.
6. If you want a different <title> tag check the "Override title tag" checkbox
   and enter the desired <title> tag. Again substitutions are allowed.
7. That's it, drag your pane to wherever you want in your layout and save.

Titles and translateable.