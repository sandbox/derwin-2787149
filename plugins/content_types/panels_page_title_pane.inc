<?php

/**
 * @file
 * Ctools content type to create a page title pane and set the page title tag.
 */

/**
 * Plugins are described by creating a $plugin array.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Panels Page Title'),
  'description' => t('Pane for the page title and title tag'),
  'category' => t('Page Title'),
  'edit form' => 'panels_page_title_edit_form',
  'render callback' => 'panels_page_title_render',
  'admin info' => 'panels_page_title_admin_info',
  'defaults' => array(
    'panels_page_title' => '',
    'panels_page_title_tag' => '',
    'markup' => 'h1',
    'class' => 'title',
    'id' => '',
    'override_title_tag' => 0,
  ),
  // This is NEEDED to be able to use substitution strings in your pane.
  'all contexts' => TRUE,
);

/**
 * An edit form for the pane's settings.
 */
function panels_page_title_edit_form($form, &$form_state) {

  $form['override_title_markup']['#markup'] = t('This is the pane title and will most likely <i>not</i> be used');

  $conf = &$form_state['conf'];

  $contexts = $form_state['contexts'];;

  $form['markup'] = array(
    '#title' => t('Title tag'),
    '#type' => 'select',
    '#description' => t('This should be set to H1 for SEO'),
    '#options' => array(
      'none' => t('- No tag -'),
      'h1' => t('h1'),
      'h2' => t('h2'),
      'h3' => t('h3'),
      'h4' => t('h4'),
      'h5' => t('h5'),
      'h6' => t('h6'),
      'div' => t('div'),
    ),
    '#default_value' => empty($conf['markup']) ? 'h1' : $conf['markup'],
  );

  $form['id'] = array(
    '#title' => t('CSS id to use'),
    '#type' => 'textfield',
    '#default_value' => empty($conf['id']) ? '' : $conf['id'],
  );

  $form['class'] = array(
    '#title' => t('CSS class to use'),
    '#type' => 'textfield',
    '#default_value' => empty($conf['class']) ? '' : $conf['class'],
  );

  $form['panels_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page title'),
    '#description' => t('Text to display, it may use substitution strings ex. %node:title. Will populate the &lt;title&gt; tag unless overridden below.'),
    '#default_value' => $conf['panels_page_title'],
  );

  $form['override_title_tag'] = array(
    '#title' => t('Override title tag'),
    '#type' => 'checkbox',
    '#default_value' => $conf['override_title_tag'],
  );

  $form['panels_page_title_tag'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Title Tag'),
    '#description' => t('Text to display in title tag, it may use substitution strings. Create a token context to get token substitions.'),
    '#default_value' => $conf['panels_page_title_tag'],
    '#states' => array(
      // Only show this field when the 'toggle_me' checkbox is enabled.
      'visible' => array(
        ':input[name="override_title_tag"]' => array('checked' => TRUE),
      ),
    ),
  );

  $rows = array();
  foreach ($contexts as $context) {
    foreach (ctools_context_get_converters('%' . check_plain($context->keyword) . ':', $context) as $keyword => $title) {
      $rows[] = array(
        check_plain($keyword),
        format_string('@identifier: @title', array('@title' => $title, '@identifier' => $context->identifier)),
      );
    }
  }

  $header = array(t('Keyword'), t('Value'));
  $form['contexts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Substitutions'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#value' => theme('table', array('header' => $header, 'rows' => $rows)),
  );

  return $form;
}

/**
 * Submit function.
 */
function panels_page_title_edit_form_submit(&$form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Run-time rendering of the body of the block (content type).
 */
function panels_page_title_render($subtype, $conf, $args, $contexts) {

  if (empty($conf['markup'])) {
    $conf['markup'] = 'h1';
  }
  if (empty($conf['class'])) {
    $conf['class'] = '';
  }

  if (empty($conf['id'])) {
    $conf['id'] = '';
  }

  if (empty($conf['panels_page_title'])) {
    $conf['panels_page_title'] = '';
  }
  else {
    // Substitue context keywords in the title.
    $title = ctools_context_keyword_substitute($conf['panels_page_title'], array(), $contexts);
    // Sanitize and translate.
    $conf['panels_page_title'] = t('!title', array('!title' => filter_xss_admin($title)));
  }

  $token = ctools_set_callback_token('title', array(
    'panels_page_title_content_type_token',
    $conf['markup'],
    $conf['id'],
    $conf['class'],
    $conf['panels_page_title'],
  )
  );

  $block = new stdClass();
  if ($token) {
    $block->content = $token;
  }

  return $block;
}

/**
 * Implements template_preprocess_html().
 */
function panels_page_title_preprocess_html(&$variables) {

  $site_name = variable_get('site_name', 'Drupal');

  $display = panels_get_current_page_display();

  if (!empty($display->content)) {
    foreach ($display->content as $key) {
      if ($key->type == 'panels_page_title_pane') {
        // If override title tag checkbox is checked
        if ($key->configuration['override_title_tag'] == 1) {
          // Substitue context keywords in the title.
          $title = ctools_context_keyword_substitute($key->configuration['panels_page_title_tag'], array(), $display->context);
          // Sanitize and translate.
          $variables['head_title'] = t('!title', array('!title' => filter_xss_admin($title)));
        }
        else {
          // Substitue context keywords in the title.
          $title = ctools_context_keyword_substitute($key->configuration['panels_page_title'], array(), $display->context);
          $title = strip_tags($title);
          // Sanitize and translate.
          $variables['head_title'] = t('!title', array('!title' => filter_xss_admin($title)));
          // Add the site name, which is already translateable.
          $variables['head_title'] .= ' | ' . $site_name;
        }
      }
    }
  }
}

/**
 * Variable token callback to properly render the page title, with markup.
 */
function panels_page_title_content_type_token(&$variables, $tag, $id, $class, $title) {
  if ($tag == 'none') {
    return $title;
  }

  $output = '<' . $tag;
  if ($id) {
    $output .= ' id="' . $id . '"';
  }

  if ($class) {
    $output .= ' class="' . $class . '"';
  }

  $output .= '>' . $title . '</' . $tag . '>' . "\n";
  return $output;
}
